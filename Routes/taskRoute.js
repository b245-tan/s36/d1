const express = require("express");
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js")

// ROUTES

// Route to getAll
router.get("/", taskController.getAll);

// Route for createTask
router.post("/addTask", taskController.createTask);

// Route for deleteTask
router.delete("/deleteTasks/:id", taskController.deleteTask)


// Route for getOne
router.get("/:id", taskController.getOne)

// Route for updateOne
router.put("/:id/complete", taskController.updateOne)





module.exports = router;