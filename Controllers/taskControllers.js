const Task = require("../Models/task.js");

// CONTROLLERS and FUNCTIONS

// Controller/function to get all the task on our database
module.exports.getAll = (request, response) => {

	Task.find({})
	// to capture the result of the find method
	.then(result => {
		return response.send(result);
	})
	// method to capture the error when find method is executed
	.catch(error => {
		return response.send(error);
	})
}


// Add Task on our Database
module.exports.createTask = (request, response) => {
	const input = request.body;

	Task.findOne({name: input.name})
	.then(result => {
		if(result !== null){
			return response.send('This task is already existing')
		} else {
			let newTask = new Task({
				name: input.name
			})

			newTask.save().then(save => {
				return response.send('The task is successfully added!')
			}).catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error);
	})
}


// Controller that will delete the document that contains the given object.

module.exports.deleteTask = (request, response) => {
	let idToBeDeleted = request.params.id;

	// findbyIdAndRemove - finds the document that contains the id and then deletes the item
	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error);
	})
}


// ACTIVITY ---------------------------------------------------------------------------------------------------

// Controller that will find and RETURN a specific object.

module.exports.getOne = (request, response) => {
	let idToBeFound = request.params.id;

	Task.findById(idToBeFound)
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error);
	})
}


// Controller that will find and UPDATE a specific object.

module.exports.updateOne = (request, response) => {
	let idToBeUpdated = request.params.id;

	Task.findByIdAndUpdate(idToBeUpdated, {status: "completed"}, {new: true})
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error);
	})
}