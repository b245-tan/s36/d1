const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

const taskRoute = require('./Routes/taskRoute.js')


	// MongoDB Connection

	mongoose.connect("mongodb+srv://admin:admin@batch245-tan.bh8imqy.mongodb.net/s35-discussion?retryWrites=true&w=majority", {
			// Allows us to avoid any current and future errors while connecting to MONGODB
			useNewUrlParser: true,
			useUnifiedTopology: true
	})



	// Check Connection
	let db = mongoose.connection;

	// Error Catcher
	db.on("error", console.error.bind(console, "Connection Error!"));

	// Confirmation of the Connection
	db.once("open", () => console.log("We are now connected to the cloud!"))



// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Routing
// locahost:3001/tasks/get
app.use("/tasks", taskRoute)




app.listen(port, () => console.log(`Server is running at port ${port}.`))


/*
	Separation of Concerns:
		- Model should be connected to the Controller.
		- Controller should be connected to the Routes.
		- Route should be connected to the Server/Application

		model > controller > server
*/